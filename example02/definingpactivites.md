# Defining Packtivities

After an introduction to what a linux container image can be used for this example hopes to familiarise you with the concept of pactivivities or packaged activities. 

### What and Why Yadage?
Yadage is two things:

  * a set of JSON schemas to describe parametrized workflows based on dynamic directed acyclic graphs (DAGs) of tasks encapsulated in pre-packaged environments (e.g. using linux containers / Docker)
  * an execution engine for such workflow definitions supporting both local and distributed execution

While there are many workflow engines already, most either make it impossible or cumbersome to run workflows, in which the complete DAG is only known at runtime. In yadage the idea of dynamic DAGs is central to the system and workflows are defined as a set of instruction, stages how to build up the DAG during run-time.

## Hello World Example
We should start with a simple example that uses only one parameter. This uses a docker image that we've used in passing before to keep a volume open. 

We define a workflow simply

```
stages:
  - name: hello_world
    dependencies: [init]
    scheduler:
      scheduler_type: singlestep-stage
      parameters:
        parone: {stages: init, output: par, unwrap: true}
        outputfile: '{workdir}/hello_world.txt'
      step:
        process:
          process_type: 'string-interpolated-cmd'
          cmd: 'echo Hello {parone} | tee {outputfile}'
        publisher:
          publisher_type: 'frompar-pub'
          outputmap:
            outputfile: outputfile
        environment:
          environment_type: 'docker-encapsulated'
          image: busybox
```

once this is in your local directory (with yadage installed by `pip install yadage`) this can be run with the command:
```
yadage-run workdir workflow.yml -p par=World
```
the first argument specifies the name of a folder in which the output of the workflow is stored. this second argument is the code defined above and the `-p` tag denotes parameters. Inside the code we see that a single step stage is defined with that step being to process a command that echos some text (including our input parameter) into an output file. The output file name could just as easily be a parameter. Finally the linux container images defined in the previous example are defined in the environment section of the step. Here we use a docker image that simply keeps the system busy whilst any other command is ran. 


## Root Flow

Previously we used a docker image to run simple generation and fitting root macros. This step now sets up each of these steps into stages in a workflow. 

first we define the steps:
```
gendata:
  process:
    process_type: 'interpolated-script-cmd'
    script: root -b -q 'gendata.C({events},"{outfilename}")'
  publisher:
    publisher_type: 'frompar-pub'
    outputmap:
      data: outfilename
  environment:
    environment_type: 'docker-encapsulated'
    image: atrisovic/rootexample

fitdata:
  process:
    process_type: 'interpolated-script-cmd'
    script: root -b -q 'fitdata.C("{data}","{outfile}")'
  publisher:
    publisher_type: 'frompar-pub'
    outputmap:
      plot: outfile
  environment:
    environment_type: 'docker-encapsulated'
    image: atrisovic/rootexample
```
which as before defines the commands, inputs and outputs and the environments.

next we insert them into a workflow:
```
stages:
  - name: gendata
    dependencies: ['init']
    scheduler:
      scheduler_type: singlestep-stage
      parameters:
        events: {stages: init, output: events, unwrap: true}
        outfilename: '{workdir}/data.root'
      step: {$ref: 'steps.yml#/gendata'}
  - name: fitdata
    dependencies: ['gendata']
    scheduler:
      scheduler_type: singlestep-stage
      parameters:
        data: {stages: gendata, output: data, unwrap: true}
        outfile: '{workdir}/plot.png'
      step: {$ref: 'steps.yml#/fitdata'}
```

Now as before there is a parameter called `events` which needs to be passed to the workflow. Instead we can add a configuration file.
```
echo 'events: 20000' > input.yml
```
Then run the workflow!
```
yadage-run workdir workflow.yml input.yml
```

the output of each stage (if any) is located in its own folder inside the working directory. In this example the final plot is produced in: `workdir/fitdata/plot.png`

## Running a more complex example

The example for the ATLAS offline software tutorial reading in xAODs and outputting root trees, reading in the root trees and providing some more complex analysis (maybe TMVA) outputting histograms then a code to read in the histograms and perform a fit