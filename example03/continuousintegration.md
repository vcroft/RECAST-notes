# Examples using continuous integration

### What is continuous integration. 
In the past, each team working on a common code were forced to agree on a proceedure to verify that each check-in on the shared repository did not 'break' the installation used by the others. Modern version control systems (e.g. gitlab) now include facilities for Continuous Integration (CI). CI requires that analysers augment their development process with an addition that allows for the automatic building and checking of the code. 

The gitlab CI process defines the environment required by the code at the check-in stage. The code is then installed within a container image to verifithat both: the code compiles/runs and; that assumed environment is all that is necessary for successful operation. Thus minimising the affect of individuals personal development setups on the code being checked in and deployed.

The containers used by these automatic builds can be discarded upon completion with a pass/fail status or attached to the code repository as a 'snapshot' of how the correctly installed code should look like. This later option is precisely what we are interested in. 

The rest of this example is split into two parts: Rel21 and beyond and pre-Rel21. Following the development of the ATLAS release 21 software environment, docker images containing the developement environment are available freely on DockerHub. This process then greatly simplifies the process described in the first example, since access to EOS and cvmfs libaries is included inside the release 21 environment. Code can then be developed using conventional CI tools. Prior to this development however the environment had to be set up manually. If your code uses any pre-release 21 software environment, it is still possible to capture the analysis in this same manner. Basically the setup used in the first example (docker containers) is repeated here to create a gitlab 'runner' which is then assigned to the repository. 

## Release 21 and beyond

This process requires adding two short scripts to the root directory of the code repository. This can be done in the GitLab browser editor and doesn't even require logging in to a terminal.

### .gitlab-ci.yml
The first code is a GitLab CI Yaml standard piece of code that merely tells gitlab that there will be a dockerfile inside the repo and it should build the image. If your repository already has a continuous integration setup these lines can simply be appended to the code (normally) without issue. The file must be called .gitlab-ci.yml

```
stages:
    - docker

build_image:
  stage: docker
  tags:
    - docker-image-build
  script:
    - echo docker image being built
```

Once committed you'll probably get an email/notification saying that the pipeline has failed. This is because there isn't a docker file yet...

### Dockerfile

The dockerfile also follows a template. It should be called `Dockerfile`

```
#
# Image providing the project on top of an AnalysisBase image.
#

# Set up the base image
FROM atlas/analysisbase:21.2.4

# Copy the projects sources into the image:
COPY . source

# Build the project inside a build/ directory:
RUN source /home/atlas/release_setup.sh && \
    mkdir build && cd build/ && cmake ../source/ && cmake --build . && cpack
```

Note that in the `MyxAODAnalysis` described in the first example, the code is already arranged into the `$HOME/source $HOME/build $HOME/run` structure depicted here. thus the copy step is simply modified to `COPY source/ source`

this docker file should mirror what you would usually do during an installation. First you set up the environment, checkout the code then build everything and can be modified accordingly. If the pipeline fails, it will give you a notification detailing how these build instructions need to be modified to suit your needs.

## Pre-release 21
As explained above, if your code was written pre-release 21 this isn't an issue. A similar proceedure to that described in the first example is needed. A runner is created on the CERN openstack system that can be assigned to a particular repository. This only needs to be done once then you can assign this runner to whichever repositories you have access.

### Set up cluster 
First we need to set up a cluster that we can dedicate to this runner. As before, we begin by logging into `lxplus-cloud` and activating your credentials. Then we create a cluster roughly following the recipe outlined here:

http://clouddocs.web.cern.ch/clouddocs/containers/tutorials/swarmgitlab.html

In this case we won't be using one of the default templates but creating our own. This will be mostly the same as `swarm` but have a larger flavor and docker volume

Note: update the image id 
Note: make sure you have enough storage or adjust the docker volume size
   
```
   magnum cluster-template-show swarm|grep image_id

    magnum cluster-template-create \  
    --name codecap-swarm-large \
    --labels cvmfs_tag=cvmfs \
    --master-flavor-id m2.large \
    --external-network-id CERN_NETWORK \
    --image-id <image id> \ 
    --volume-driver rexray \
    --flavor-id m2.large \
    --dns-nameserver 137.138.17.5 \
    --coe swarm --docker-volume-size 300
```
Now we start this cluster

```
    magnum cluster-create --name gitlabci-swarm --keypair-id openstack --cluster-template codecap-swarm-large --node-count 1
```

It's best to make a directory for this to keep the setup safe in case you need to shut down and restart it at some point.
```
    mkdir magnum && cd magnum
    magnum cluster-config gitlabci-swarm > setup.sh
    source setup.sh
```

For this case we shall use the gitlab specific runner image
```
    docker pull gitlab/gitlab-runner
```

and assign a volume to it together with the standard cvfms pluggins.
```
    docker volume create --name gitlab-runner-config
    docker volume create -d cvmfs --name=atlas-condb.cern.ch
    docker volume create -d cvmfs --name=atlas.cern.ch
    docker volume create -d cvmfs --name=sft.cern.ch
```

make a data volume to contain all our secrets
```
    docker run -d --name datavol -v gitlab-runner-config:/etc/gitlab-runner busybox tail -f /dev/null
    docker cp ca.pem datavol:/etc/gitlab-runner
    docker cp cert.pem datavol:/etc/gitlab-runner
    docker cp key.pem datavol:/etc/gitlab-runner
```

In order to allow gitlab to veiw our runner we need to find it's api address
```
    magnum cluster-show codecap-swarm|grep api_address
```

### Run the image

Unlike previously this image won't be run interactively (with bash) this will be started to run in the background and then accessed afterwards.
```
    docker run -d --restart always --name gitlab-runner -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner
    docker exec -it  gitlab-runner bash
```

Now we register this runner with the gitlab api
```
    gitlab-ci-multi-runner register -r <your GitLab CI runner token> \
    --name codecapture-runners --tag-list code-capture --limit 1 \
    --url https://gitlab.cern.ch/ci --executor docker \
    --docker-host <the docker host from grep above> --docker-image centos:7 \
    --docker-disable-cache --docker-cert-path /etc/gitlab-runner
```

this command should have created a configuration file called `config.toml` that we need to edit a bit more so let's copy it back to lxplus:

```
    docker cp datavol:/etc/gitlab-runner/config.toml newconfig.toml
```    
this `config.toml` should be changed to look like this:
```
    volumes = ["/cache","/var/run/docker.sock:/var/run/docker.sock"]
    tls_verify = true 
    privileged = true
```
and then we copy it back
```
    docker cp newconfig.toml datavol:/etc/gitlab-runner/config.toml
```

you can check the status of the runner via (it should automatically re-load the config)
```
    docker logs -f gitlab-runner 
```

on your GitLab CI page you should see the runner being available. (on Settings / Pipelines)


## Setting gitlab-ci variables
As in the release 21 example above, the repository on gitlab needs two small code additions to instruct the system how to build the image. We need a gitlab-ci template to start the build and a dockerfile with instructions how the image should be built.

### pre-rel21 .gitlab-ci.yml
First in main directory of the repository to be built must be a file called `.gitlab-ci.yml`. This instructs gitlab to search for a runner and assign the docker file to it. The file should contain the following lines:
```
codecapture:
  tags: [code-capture]
  variables:
    BUILD_BASE_IMAGE: lukasheinrich/recast_cvmfs_assisted
    TO: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}
  image: gitlab-registry.cern.ch/runner_admin/codecapture_steering
  script:
   - /codecapture_utils/steering.sh
```

### docker build script
Next in that same directory the runner will look for a file called `docker.build.sh`. This file con exactly mirror the standard setup, checout and compile steps of your standard installation as it will be executed as a shell script.
e.g.
```
echo "::: start preservation :::"

lsetup "cmake 3.7.0"
lsetup python
lsetup gcc
lsetup "gsl 2.1-x86_64-slc6-gcc49-opt"
lsetup "fftw 3.3.4-x86_64-slc6-gcc49-opt"
lsetup root
echo ":: Done setting up ::" 
git clone https://:@gitlab.cern.ch:8443/atlas-repo/AnalysisCode.git
cd AnalysisCode
git clone https://:@gitlab.cern.ch:8443/uname/miscSVN.git
mv miscSVN/* ./
rm -rf miscSVN
git clone https://:@gitlab.cern.ch:8443/MultiBJets/NNLOReweighter.git NNLOReweighter


# perform the build
mkdir build
cd build
cmake ..
make -j2

# prepare to execute the analysis
cd ..
cd AnalysisRun/run
echo "::: end preservation :::"

```

This code is very flexible however issues have arrisen due to code still living on SVN. In the above example, all the relevant code that still lives on SVN has been checked out to lxplus and then commited into a directory called miscSVN on the users gitlab. All of the relevant packages are then unpacked and movedas normal. 

This stage can be used to checkout several scripts meaning that the bulk of the development code can remain at it's initial destination and this repo need only contain the ci yaml and the dockerfile.

## CMake compatibility.
This second example does not require cmamke to build the package. Any setup script can be used. Even if you are using release 21 if your build is not CMake compatible then it is necessary to first set up a runner like this.  

