# Introduction To Linux Containers and the CERN OpenStack Service on LXPLUS

This first exercise gives a first introduction to using Linux Container Images. In this field Docker has quickly become one of the premier projects for containerizing applications. This excercise provides a hands-on approach to start using Docker using lxplus and the [CERN OpenStack](https://clouddocs.web.cern.ch/clouddocs/) facilities.

The default lxplus environment often has older OpenStack client versions due to dependency issues.

A lxplus cloud flavor with the latest versions has been provided. Use it if you need the new functionality or clients not available in the default lxplus.
```
ssh lxplus-cloud.cern.ch
```
or alternatively
```
ssh lxplus001.cern.ch
```

which gives:
```
* ********************************************************************
* Welcome to lxplus001.cern.ch, CentOS, 7.3.1611
* Archive of news is available in /etc/motd-archive
* Reminder: You have agreed to comply with the CERN computing rules
* https://cern.ch/ComputingRules
* Puppet environment: production, Roger state: production
* Foreman hostgroup: lxplus/nodes/cloud
* ************** INFO INFO ****************
 lxplus001.cern.ch is a special lxplus node with software installed
to access particular cloud services, it is not part of the general
lxplus.cern.ch alias. For any support requests please
mention that this node is member of lxplus-cloud.cern.ch
************************************************
* LXPLUS Public Login Service
* ********************************************************************
```

## The CERN cloud container service
The CERN cloud container service allows you to create container clusters.

Supported technologies include Docker Swarm, Kubernetes and Mesos. But we will focus initially on Docker.

This information has been extracted from [the cloud docs](https://clouddocs.web.cern.ch/clouddocs/).

### Environment

[Check here](https://clouddocs.web.cern.ch/clouddocs/clients/lxplus.html) to get access to a magnum client. These instructions will allow you to subscribe to the cloud service and set up a configuration for your lxplus shell profile. This uses kerberos authentification to allow the construction of an OpenStack keypair. This should be placed somewhere you can find it.

### Create a cluster

For more detailed documentation check the [upstream docs.](http://docs.openstack.org/project-install-guide/container-infrastructure-management/newton/launch-instance.html)

A set of public cluster templates have been defined for you to reuse:

```
$ magnum cluster-template-list
-------+--------------------+
| uuid | name               |
+------+--------------------+
| .... | swarm              |
| .... | swarm-preview      |
| .... | kubernetes         |
| .... | kubernetes-preview |
| .... | mesos              |
| .... | mesos-preview      |
+------+--------------------+
```

Here's the sequence of commands to launch a new swarm cluster:
```
$ magnum cluster-create --name myswarmcluster --keypair-id mykey \
    --cluster-template swarm --node-count 2

$ magnum cluster-list
+--------------------------------------+----------------+------------+--------------+--------------------+
| uuid                                 | name           | node_count | master_count | status             |
+--------------------------------------+----------------+------------+--------------+--------------------+
| 98b642fa-c2a5-11e6-b663-68f728b18b2d | myswarmcluster | 2          | 1            | CREATE_IN_PROGRESS |
+--------------------------------------+----------------+------------+--------------+--------------------+
```

Once the cluster is in CREATE_COMPLETE state (only a couple minutes), you can start using it:

```
$ $(magnum cluster-config myswarmcluster)

docker ps
docker info
```
If you want to easily reuse this config later, do instead:
```
$ magnum cluster-config myswarmcluster > env.sh

. env.sh
docker ps
```

You can see and manage your active openstack instances and more on [the openstack dashboard](https://openstack.cern.ch/project/)

## Running a Linux Container Image

first we start a container image. Let's use a simple python. 
```
docker run -it python bin/bash
```
this command tells  docker to run interactively (the -i tag) the python container (this could be anything. If the container doesn't exist localy then it will download it from the hub. The last item is the command to run. bin/bash just runs the interactive shell. 

This will open a new container with the python engine running inside and very little else. Let's add a script:
```
echo "print('hello world I am in a container')" > pythonhelloworld.py

python pythonhelloworld.py
```
And there it is! Type exit or ctl-d to exit the container. 

Note: if you run this same command again the python script you created will not be inside. However, by running ```docker ps -a``` it's possible to see the name of the container that you previously exited and this name cna be substitued in place of the ```python``` part of the docker run command above. 

## Adding files to a linux container image and executing non-interactively: aka, working with Dockerfiles.

It's possible to automate all of these steps. In this folder we can create a file called ```Dockerfile```

First we define which container to run. The first like in ```Dockerfile``` should be:
```
FROM: python
```

Next we add the script (you can create this using the echo command above or through an editor or anyway you normally make files on lxplus) to the docker container. 

```
ADD pythonhelloworld.py /
```
Finally we add a line to execute the script:
```
CMD ["python", "./pythonhelloworld.py"]
```

The complete file should now look like this:
```
FROM python

ADD pythonhelloworld.py /

CMD ["python", "./pythonhelloworld.py"]
```

So now we can build our image. 
```
docker build -t python-hello-world .
```
Did it work? let's run it and find out. 
```
docker run python-hello-world
```

## A ROOT macro
There is a lot of subtleties that can be lost when explaining what a Linux Container Image is. In the previous example we built our code on top of a very bare python installation. It is possible to build your container image from more complicated starting point. In our next step we want to run ROOT. 

Several ROOT images exist. In this example docker will download an image from from the Docker Hub that was built by user atrisovic which has some nice code already inside.
```
docker run -it atrisovic/rootexample bash
```
Again this runs the container in interactive mode giving access to the files that are contained.

This container is more comlpex and contains acess to the ```less``` command. There are two example macros contained in the ```code``` folder. ```gendata.C``` and ```fitdata.C``` which generate and fit data respectively. You can inspect these files yourself to make sure nothing funny is going on. 
```
less gendata.C
```
once you are convinced they can be run interactively using the normal commands. 
```
root -b -q 'gendata.C(2000,"data.root")'

root -b -q 'fitdata.C("data.root","plot.png")'
```
The first macro takes a number of events as input and outputs a file with a given name. The second macro reads in a file with a given name and outputs a plot with another given name. 


## reading ROOT output
Here we get to the crux of how to make container systems such as these useful. The ROOT image provided above contains the environment and all the code necesarry to produce data and analyse that data and it can be ran on any machine that has docker. No need to install ROOT. However just like connecting to an external cluster such as lxplus, the files are still stored in the container. 

In this next example first we build a Dockerfile that runs the root commands as before. Then we add a volume such that the files produced by the program are stored locally. 
Let's make a dockerfile that runs the production. In a new folder make a new file called `Dockerfile` containing:
```
FROM atrisovic/rootexample

CMD root -b -q 'gendata.C(2000,"data.root")'
```
Then we build. 
```docker build -t ROOTgen .```

This operation runs the generate command but the file it generates is still inside the container. 


## Adding layers. running CVMFS.
The examples given so far are fairly trivial and do not need access to the CERN openstack facilities. This next example runs a recent ATLAS offline software tutorial and therefore requires special ATLAS permissions and software that is not common on peoples personal machines. The proceedure is exactly the same.

Using the existing cluster that we have running we need a volume inwhich to store all of the credentials used for downloading code and accessing files etc
```
$ docker volume create --name secrets
$ docker run -d --name secrets -v secrets:/secrets busybox tail -f /dev/null
```
This creates a virtual volume which we then link to a persistant volume in a busy background container called secrets
then we copy/create a ssh config file like this:
```
$ cat ~/.ssh/config
Host svn.cern.ch
  User uname
  GSSAPIAuthentication yes
  GSSAPIDelegateCredentials yes
  Protocol 2
  ForwardX11 no
```
then we copy the whole dir into the secrets volume
```
$ docker cp ~/.ssh secrets:/secrets/uname_ssh
```

ok cool. If that all works then we can start the other cvmfs containers
```
$ docker volume create -d cvmfs --name=atlas-condb.cern.ch
$ docker volume create -d cvmfs --name=atlas.cern.ch
$ docker volume create -d cvmfs --name=sft.cern.ch
```
ok cool big moment now. It's time to run the docker cvmfs container
```
docker run --name atlasoffline -it --security-opt label:disable -v secrets:/secrets -v atlas-condb.cern.ch:/cvmfs/atlas-condb.cern.ch -v atlas.cern.ch:/cvmfs/atlas.cern.ch -v sft.cern.ch:/cvmfs/sft.cern.ch lukasheinrich/recast_cvmfs_assisted:20170416 bash
```
notice that the volumes we created are attached using the ```-v``` label. The full name ```lukasheinrich/recast_cvmfs_assisted:20170416``` is necessary since otherwise docker will not know where to find the cvmfs container.

### prepare run container and check out code
```
$ kinit uname
$ cp /secrets/uname_ssh/* ~/.ssh/
$ ssh-keyscan -p 7999 -H gitlab.cern.ch >> ~/.ssh/known_hosts
$ export SVNROOT=svn+ssh://uname@svn.cern.ch/reps/atlasoff
$ export CERN_USER=uname
$ mkdir -p /code/ROOTAnalysisTutorial && cd /code/ROOTAnalysisTutorial
```
where of course uname is your username. Next we clone the code exactly as above.

Now we can perform the software tutorial exactly as intended
```
lsetup 'rcsetup Base,2.4.28'
rc find_packages
rc compile
...etc...
```

or checkout and build the code found (here)[https://gitlab.cern.ch/vcroft/MyxAODAnalysis]. This needs authorisation.
```
kinit uname
cp /secrets/uname_ssh/* ~/.ssh/
ssh-keyscan -p 7999 -H gitlab.cern.ch >> ~/.ssh/known_hosts
export SVNROOT=svn+ssh://uname@svn.cern.ch/reps/atlasoff
export CERN_USER=uname
```

code checked out as normal
```
git clone https://:@gitlab.cern.ch:8443/vcroft/MyxAODAnalysis.git
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
export ALRB_TutorialData=$PWD
rm -rf build
mkdir build
cd build
asetup 21.2.0,AnalysisBase
source ${AnalysisBase_PLATFORM}/setup.sh
cmake ../source
make
source ${AnalysisBase_PLATFORM}/setup.sh




Any custom algorithms that you create and compile will be stored inside this container. However since the ASG release found on CVMFS is so large currently the only way to access the ASG algorithms (such as EventLoop) is to run the system on a CVMFS compatable system. This means running the commands above to create the 4 volumes (secrets and 3 x CVMFS) are necessary every time we want to run the system. 

regardless. Before exiting the container the secrets from your account should be destroyed.
```
unset SVNROOT
unset CERN_USER
kdestroy
yes|rm -rf ~/.ssh/*
rm -rf /tmp/krb*
```